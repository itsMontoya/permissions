package permissions

import (
	"encoding/json"
	"path"

	"github.com/Hatch1fy/errors"
	"github.com/boltdb/bolt"
)

const (
	// ErrUninitialized is returned when the library has not been proper initialized
	ErrUninitialized = errors.Error("library has not been initialized")
	// ErrInvalidActions is returned when an invalid permissions value is attempted to be set
	ErrInvalidActions = errors.Error("invalid permissions, please see constant block for reference")
	// ErrPermissionsUnchanged is returned when matching permissions are set for a resource
	ErrPermissionsUnchanged = errors.Error("permissions match, unchanged")
	// ErrResourceNotFound is returned when a requested resource cannot be found
	ErrResourceNotFound = errors.Error("resource not found")
	// ErrGroupNotFound is returned when a requested group cannot be found
	ErrGroupNotFound = errors.Error("group not found")
)

var (
	resourceBktKey = []byte("resource")
	groupsBktKey   = []byte("groups")
)

// New will return a new instance of Permissions
func New(dir string) (pp *Permissions, err error) {
	var p Permissions
	if p.db, err = bolt.Open(path.Join(dir, "permissions.bdb"), 0644, nil); err != nil {
		return
	}

	if err = p.init(); err != nil {
		return
	}

	pp = &p
	return
}

// Permissions manages permissions
type Permissions struct {
	db *bolt.DB
}

func (p *Permissions) init() (err error) {
	return p.db.Update(func(txn *bolt.Tx) (err error) {
		if _, err = txn.CreateBucketIfNotExists(resourceBktKey); err != nil {
			return
		}

		_, err = txn.CreateBucketIfNotExists(groupsBktKey)
		return
	})
}

func (p *Permissions) getResource(txn *bolt.Tx, id []byte) (r resource, err error) {
	var (
		bkt *bolt.Bucket
		bs  []byte
	)

	if bkt = txn.Bucket(resourceBktKey); bkt == nil {
		err = ErrUninitialized
		return
	}

	if bs = bkt.Get(id); bs == nil {
		err = ErrResourceNotFound
		return
	}

	if err = json.Unmarshal(bs, &r); err != nil {
		return
	}

	return
}

func (p *Permissions) getGroups(txn *bolt.Tx, userID []byte) (g groups, err error) {
	var (
		bkt *bolt.Bucket
		bs  []byte
	)

	if bkt = txn.Bucket(groupsBktKey); bkt == nil {
		err = ErrUninitialized
		return
	}

	if bs = bkt.Get(userID); bs == nil {
		err = ErrGroupNotFound
		return
	}

	if err = json.Unmarshal(bs, &g); err != nil {
		return
	}

	return
}

func (p *Permissions) putResource(txn *bolt.Tx, id []byte, r resource) (err error) {
	var bkt *bolt.Bucket
	if bkt = txn.Bucket(resourceBktKey); bkt == nil {
		err = ErrUninitialized
		return
	}

	var bs []byte
	if bs, err = json.Marshal(r); err != nil {
		return
	}

	if err = bkt.Put(id, bs); err != nil {
		return
	}

	return
}

func (p *Permissions) putGroups(txn *bolt.Tx, id []byte, g groups) (err error) {
	var bkt *bolt.Bucket
	if bkt = txn.Bucket(groupsBktKey); bkt == nil {
		err = ErrUninitialized
		return
	}

	var bs []byte
	if bs, err = json.Marshal(g); err != nil {
		return
	}

	if err = bkt.Put(id, bs); err != nil {
		return
	}

	return
}

// Get will get the permissions for a given group for a resource id
func (p *Permissions) Get(id, group string) (actions Action) {
	var r resource
	p.db.View(func(txn *bolt.Tx) (err error) {
		if r, err = p.getResource(txn, []byte(id)); err != nil {
			return
		}

		actions, _ = r.Get(group)
		return
	})

	return
}

// SetPermissions will set the permissions for a resource id being accessed by given group
func (p *Permissions) SetPermissions(resourceID, group string, actions Action) (err error) {
	var r resource
	if !isValidActions(actions) {
		return ErrInvalidActions
	}

	idBytes := []byte(resourceID)

	return p.db.Update(func(txn *bolt.Tx) (err error) {
		if r, err = p.getResource(txn, idBytes); err != nil {
			if err != ErrResourceNotFound {
				return
			}

			r = make(resource)
			err = nil
		}

		if !r.Set(group, actions) {
			return ErrPermissionsUnchanged
		}

		err = p.putResource(txn, idBytes, r)
		return
	})
}

// UnsetPermissions will remove the permissions for a resource id being accessed by given group
func (p *Permissions) UnsetPermissions(resourceID, group string) (err error) {
	return p.db.Update(func(txn *bolt.Tx) (err error) {
		var r resource
		idBytes := []byte(resourceID)
		if r, err = p.getResource(txn, idBytes); err != nil {
			if err != ErrResourceNotFound {
				return
			}

			r = make(resource)
			err = nil
		}

		r.Remove(group)

		err = p.putResource(txn, idBytes, r)
		return
	})
}

// AddGroup will add a group to a userID
func (p *Permissions) AddGroup(userID string, grouplist ...string) (err error) {
	var g groups

	userIDBytes := []byte(userID)

	return p.db.Update(func(txn *bolt.Tx) (err error) {
		if g, err = p.getGroups(txn, userIDBytes); err != nil {
			if err != ErrGroupNotFound {
				return
			}

			g = make(groups)
			err = nil
		} else {
			g = g.Dup()
		}

		updated := false
		for _, group := range grouplist {
			if g.Set(group) {
				updated = true
			}
		}

		if !updated {
			return ErrPermissionsUnchanged
		}

		return p.putGroups(txn, userIDBytes, g)
	})
}

// RemoveGroup will remove a group to a userID
func (p *Permissions) RemoveGroup(userID string, grouplist ...string) (err error) {
	var g groups

	userIDBytes := []byte(userID)

	return p.db.Update(func(txn *bolt.Tx) (err error) {
		if g, err = p.getGroups(txn, userIDBytes); err != nil {
			return ErrPermissionsUnchanged
		}

		g = g.Dup()
		updated := false

		for _, group := range grouplist {
			if g.Remove(group) {
				updated = true
			}
		}

		if !updated {
			return ErrPermissionsUnchanged
		}

		return p.putGroups(txn, userIDBytes, g)
	})
}

// Can will return if a user (userID) can perform a given action on a provided resource id
func (p *Permissions) Can(userID, resourceID string, action Action) (can bool) {
	var (
		g   groups
		r   resource
		err error
	)

	userIDBytes := []byte(userID)
	idBytes := []byte(resourceID)

	if err = p.db.View(func(txn *bolt.Tx) (err error) {
		if g, err = p.getGroups(txn, userIDBytes); err != nil {
			return
		}

		if r, err = p.getResource(txn, idBytes); err != nil {
			return
		}

		switch action {
		case ActionNone:
			can = true

		case ActionRead:
			can = g.ForEach(r.canRead)

		case ActionWrite:
			can = g.ForEach(r.canWrite)

		case ActionDelete:
			can = g.ForEach(r.canDelete)
		}

		return
	}); err != nil {
		return
	}

	return
}

// Has will return whether or not an ID has a particular group associated with it
func (p *Permissions) Has(id, group string) (ok bool) {
	var g groups
	p.db.View(func(txn *bolt.Tx) (err error) {
		if g, err = p.getGroups(txn, []byte(id)); err != nil {
			return
		}

		ok = g.Has(group)
		return
	})

	return
}

// Groups will return a slice of the groups a user belongs to
func (p *Permissions) Groups(userID string) (gs []string, err error) {
	var g groups

	err = p.db.View(func(txn *bolt.Tx) (err error) {
		if g, err = p.getGroups(txn, []byte(userID)); err != nil {
			return
		}

		gs = g.Slice()
		return
	})

	return
}

// Close will close permissions
func (p *Permissions) Close() (err error) {
	return p.db.Close()
}
