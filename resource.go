package permissions

type resource map[string]Action

// Get will get the actions available to a given group
func (r resource) Get(group string) (actions Action, ok bool) {
	actions, ok = r[group]
	return
}

// Set will set the actions available to a given group
func (r resource) Set(group string, actions Action) (ok bool) {
	var currentActions Action
	currentActions, _ = r.Get(group)
	if currentActions|actions == currentActions {
		return false
	}

	r[group] = actions
	return true
}

func (r resource) canRead(group string) bool {
	act := r[group]
	return act&ActionRead != 0
}

func (r resource) canWrite(group string) bool {
	act := r[group]
	return act&ActionWrite != 0
}

func (r resource) canDelete(group string) bool {
	act := r[group]
	return act&ActionDelete != 0
}

func (r resource) Remove(group string) (ok bool) {
	if _, ok = r.Get(group); ok {
		delete(r, group)
	}

	return
}
